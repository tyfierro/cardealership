class CreateFinances < ActiveRecord::Migration
  def change
    create_table :finances do |t|
      t.decimal :interest_rate
      t.decimal :sales_tax

      t.timestamps null: false
    end
  end
end
