class AddQuotePriceToFinance < ActiveRecord::Migration
  def change
    add_column :finances, :quote_price, :decimal
  end
end
