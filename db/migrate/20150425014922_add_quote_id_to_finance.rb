class AddQuoteIdToFinance < ActiveRecord::Migration
  def change
    add_column :finances, :quote_id, :integer
  end
end
