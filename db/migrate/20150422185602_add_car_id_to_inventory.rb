class AddCarIdToInventory < ActiveRecord::Migration
  def change
    add_column :inventories, :car_id, :integer
  end
end
