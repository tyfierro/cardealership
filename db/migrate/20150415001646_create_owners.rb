class CreateOwners < ActiveRecord::Migration
  def change
    create_table :owners do |t|
      t.decimal :total_gross_revenue
      t.decimal :net_profit

      t.timestamps null: false
    end
  end
end
