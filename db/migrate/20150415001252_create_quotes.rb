class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :car_id
      t.integer :customer_id
      t.integer :staff_id
      t.decimal :price
      t.time :quote_date

      t.timestamps null: false
    end
  end
end
