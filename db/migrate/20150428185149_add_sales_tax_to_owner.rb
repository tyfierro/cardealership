class AddSalesTaxToOwner < ActiveRecord::Migration
  def change
    add_column :owners, :sales_tax, :decimal
  end
end
