class AddCustomerIdToFinances < ActiveRecord::Migration
  def change
    add_column :finances, :customer_id, :integer
  end
end
