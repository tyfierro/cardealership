class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.string :make
      t.string :model
      t.string :color
      t.string :vin
      t.decimal :whole_sale_price
      t.string :status

      t.timestamps null: false
    end
  end
end
