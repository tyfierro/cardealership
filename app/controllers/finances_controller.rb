class FinancesController < ApplicationController
  before_action :set_finance, only: [:show, :edit, :update, :destroy]

  # GET /finances
  # GET /finances.json
  def index
  @finances = Finance.all

  end

  # GET /finances/1
  # GET /finances/1.json
  def show
    @price_3_array = @finance.calculate_finance_table
    @price_4_array = @finance.calculate_finance_table
    @price_5_array = @finance.calculate_finance_table
  end

  # GET /finances/new
  def new
    @finance = Finance.new
  end

  # GET /finances/1/edit
  def edit
  end

  # POST /finances
  # POST /finances.json
  def create
    @finance = Finance.new(finance_params)

    respond_to do |format|
      if @finance.save
        format.html { redirect_to @finance, notice: 'Finance was successfully created.' }
        format.json { render :show, status: :created, location: @finance }
      else
        format.html { render :new }
        format.json { render json: @finance.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /finances/1
  # PATCH/PUT /finances/1.json
  def update
    respond_to do |format|
      if @finance.update(finance_params)
        format.html { redirect_to @finance, notice: 'Finance was successfully updated.' }
        format.json { render :show, status: :ok, location: @finance }
      else
        format.html { render :edit }
        format.json { render json: @finance.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /finances/1
  # DELETE /finances/1.json
  def destroy
    @finance.destroy
    respond_to do |format|
      format.html { redirect_to finances_url, notice: 'Finance was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def finance_table
    interest = params[:interest]
    tax = params[:tax]
    car_price = params[:car]
    @price_3_array = Finance.calculate_finance_table(quote_price.to_i, tax.to_i, interest.to_f)
    @price_4_array = Finance.calculate_finance_table(quote_price.to_i, tax.to_i, interest.to_f)
    @price_5_array = Finance.calculate_finance_table(quote_price.to_i, tax.to_i, interest.to_f)
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_finance
      @finance = Finance.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def finance_params
      params.require(:finance).permit(:interest_rate, :sales_tax, :customer_id, :quote_id, :quote_price)
    end
end
