class Car < ActiveRecord::Base
  has_many :quotes


  def self.available
    Car.where(status: 'Available')
  end

  def price_cal
    self.price = (whole_sale_price * 1.1);
  end

  def car_info
    "#{make}, #{model}, #{color}, #{status}"
  end



end
