class Quote < ActiveRecord::Base
  belongs_to :customer
  belongs_to :car
  belongs_to :staff
  has_many :finances
  has_one :owner

  def quote_price
      "#{price}"
  end
end




