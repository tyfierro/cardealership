class Owner < ActiveRecord::Base
  belongs_to :quote

  def tax
    self.sales_tax = 0.043
  end
  def total
    self.total_gross_revenue = sum.Quote.quote_price
  end

  def net
    self.net_profit = total - (total * tax)
  end
end
