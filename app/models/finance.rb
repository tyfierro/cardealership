class Finance < ActiveRecord::Base
  has_many :customers
  belongs_to :quote



  def tax
    self.sales_tax = 0.043
  end




  def self.calculate_finance_table(quote_price = 0,interest_rate = 0,sales_tax = 0.043)
    Rails.logger.debug "Inputs: #{quote_price} #{interest_rate} #{sales_tax}"
    price_3 = []

      price_3 << ((quote_price + (quote_price * sales_tax))*interest_rate * (1+ (interest_rate) * 3))/(12 * 3)
    price_4 = []
      price_4 << ((quote_price + (quote_price * sales_tax))*interest_rate * (1+ (interest_rate) * 4))/(12 * 4)
    price_5 = []
      price_5 << ((quote_price + (quote_price * sales_tax))*interest_rate * (1+ (interest_rate) * 5))/(12 * 5)

    return price_3
    return price_4
    return price_5
  end



  def calculate_finance_table
    return self.class.calculate_finance_table(self.quote_price,self.interest_rate,self.sales_tax)
  end
end

