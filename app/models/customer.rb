class Customer < ActiveRecord::Base
  belongs_to :staff
  belongs_to :finance
  has_many :quotes
  def customer
    "#{name}, #{address}, #{city}, #{state}, #{zip}, #{phone}"
  end



end
