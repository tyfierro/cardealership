class Staff < ActiveRecord::Base
  has_many :quotes
  has_many :customers

  def staff_info
    "#{name}, #{position}"
  end
  def self.salesperson
    Staff.where(position: 'Salesperson')

  end

end
