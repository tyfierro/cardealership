## Place all the behaviors and hooks related to the matching controller here.
## All this logic will automatically be available in application.js.
## You can use CoffeeScript in this file: http://coffeescript.org/
#
updatequoteprice = ->
  selection_id = $('#finance_customer_id').val()
  $.getJSON '/quotes/' + selection_id + '/price', {},(json, response) ->
    $('#finance_quote_price').val( json)

$ ->
  $('#finance_quote_id').change -> updatequoteprice()

#build_table = ->
#  car_price =  $("#calculator_car_price").val()
#  interest_rate = $("#calculator_interest_rate").val()
#  sales_tax = $("#calculator_sales_tax").val()
#  price_3 = $("#calculator_price_3").val()
#  price_4 = $("#calculator_price_4").val()
#  price_5 = $("#calculator_price_5").val()
#  $.get "/finances/finance_table",{car: car_price,interest: interest_rate, tax: sales_tax, year3: price_3, year4: price_4, year5: price_5}
#
#$ ->
#  $('#calculator_car_price,#calculator_interest_rate,#calculator_sales_tax,#calculator_price_3,#calculator_price_4,#calculator_price_5').bind 'keyup mouseup mousewheel', ->
#    build_table()
#
#  build_table()
#

