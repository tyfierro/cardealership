# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

updateprice = ->
  selection_id = $('#quote_car_id').val()
  $.getJSON '/cars/' + selection_id + '/price_cal', {},(json, response) ->
    $('#quote_price').val( json)

$ ->
  $('#quote_car_id').change -> updateprice()




updatecustomer = ->
  selection_id = $('#quote_customer_id').val()
  $.getJSON '/customers/' + selection_id + '/name', {}, (json, response) ->
    $('#quote_name').val( json['name'])

$ ->
  $('#quote_customer_id').change -> updatecustomer()