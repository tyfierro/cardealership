// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or any plugin's vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require jquery.turbolinks
//= require turbolinks
//= require_tree .

$('li').click(function(){

    $(this).addClass('active')
        .siblings()
        .removeClass('active');

});

/*

 Inspired by Asif Aleem's work: http://www.freebiesgallery.com/responsive-website-navigation/

 Feel free to fork it and make it better, AND use it however u want, the whole functionality could, of course, have been done with jQuery alone, but I thought I'd try to do it with CSS3, to make use of media query transitions, and of course, just to practice more.
 You can use this when changing orientation on a tablet or smart phone for example.. and of course, use ur creative minds for any more uses u can come up with :)

 Follow me on Twitter: http://twitter.com/SaraSoueidan

 */
