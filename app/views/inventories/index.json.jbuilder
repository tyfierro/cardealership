json.array!(@inventories) do |inventory|
  json.extract! inventory, :id, :status
  json.url inventory_url(inventory, format: :json)
end
