json.array!(@owners) do |owner|
  json.extract! owner, :id, :total_gross_revenue, :net_profit
  json.url owner_url(owner, format: :json)
end
