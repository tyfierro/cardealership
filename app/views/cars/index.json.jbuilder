json.array!(@cars) do |car|
  json.extract! car, :id, :make, :model, :color, :vin, :whole_sale_price, :price_cal, :status
  json.url car_url(car, format: :json)
end
