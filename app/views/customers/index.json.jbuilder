json.array!(@customers) do |customer|
  json.extract! customer, :id, :name, :address, :city, :state, :zip, :phone
  json.url customer_url(customer, format: :json)
end
