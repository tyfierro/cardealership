json.array!(@finances) do |finance|
  json.extract! finance, :id, :interest_rate, :sales_tax, :quote_id
  json.url finance_url(finance, format: :json)
end
