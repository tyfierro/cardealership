json.array!(@quotes) do |quote|
  json.extract! quote, :id, :car_id, :customer_id, :staff_id, :price, :quote_date
  json.url quote_url(quote, format: :json)
end
